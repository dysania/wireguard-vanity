use clap::Parser;

#[derive(Parser, Debug)]
pub struct Args {
    #[clap(required(true))]
    pub prefixes: Vec<String>,

    #[clap(short)]
    pub threads: Option<usize>
}