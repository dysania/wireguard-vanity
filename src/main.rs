use arrayvec::ArrayString;
use clap::Parser;
use rand_chacha::ChaCha8Rng;
use rand_core::OsRng;
use rand_core::SeedableRng;
use std::collections::VecDeque;
use std::sync::atomic::{AtomicU64, Ordering::SeqCst};
use std::sync::mpsc::{channel, Sender};
use std::sync::{Arc, RwLock};
use std::thread::{available_parallelism, spawn};

mod args;

static SEED: AtomicU64 = AtomicU64::new(0);

fn main() {
    let args = args::Args::parse();
    let args = Box::leak(Box::new(args));

    let mut todo: VecDeque<ArrayString<8>> = VecDeque::new();
    for p in &args.prefixes {
        todo.push_back(ArrayString::from(p).unwrap());
    }
    let todo = Arc::new(RwLock::new(todo));

    let (sender, recv): (Sender<KeyPair>, _) = channel();

    let mut threads = Vec::new();
    for _ in 0..args
        .threads
        .unwrap_or_else(|| available_parallelism().unwrap().get())
    {
        threads.push({
            let todo = Arc::clone(&todo);
            let sender = sender.clone();
            spawn(move || {
                let mut left_todo = todo.read().unwrap().len();
                while left_todo > 0 {
                    use x25519_dalek::{PublicKey, StaticSecret};

                    let finding = todo.read().unwrap().get(0).map(|s| *s);
                    let finding = if let Some(f) = finding {
                        f
                    } else {
                        break;
                    };

                    let private =
                        StaticSecret::new(ChaCha8Rng::seed_from_u64(SEED.fetch_add(69, SeqCst)));
                    let public = PublicKey::from(&private);

                    let public_base64 = base64::encode(public.as_bytes());

                    if public_base64.starts_with(&finding[..]) {
                        sender
                            .send(KeyPair {
                                public: public_base64,
                                private: base64::encode(private.to_bytes()),
                            })
                            .unwrap();
                    }

                    left_todo = todo.read().unwrap().len();
                }
            })
        });
    }
    drop(sender);

    for kp in recv {
        println!("public: {}\tprivate: {}", kp.public, kp.private);
        todo.write().unwrap().pop_front();
    }

    for t in threads.drain(..) {
        t.join().unwrap();
    }
}

#[derive(Debug)]
struct KeyPair {
    public: String,
    private: String,
}
